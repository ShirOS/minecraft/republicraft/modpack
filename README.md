# Republicraft Modpack

## Table of Contents

[[_TOC_]]

## Information

Vous pouvez accéder à la documentation en [cliquant ici](https://gitlab.com/shiros/minecraft/republicraft/modpack/-/wikis/home).

## Versions du Modpack

- 1.6.4
- 1.12.2
- 1.18.2

Chacune des versions est disponible sur le repository.
Pour selectionner celle que vous souhaitez, changez la branche master en la version souhaitée.

## Authors

- [Alexandre Caillot (Shiroe_sama)](https://gitlab.com/Shiroe_sama)
